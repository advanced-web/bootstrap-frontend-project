import { Container, Stack } from "react-bootstrap";
import { createBrowserRouter, RouterProvider } from "react-router-dom";
import routes from "./features/routes";

const router = createBrowserRouter(routes);

function App() {
	return (
		<Container fluid className="main-container">
			<Stack direction="vertical" className="justify-content-center align-items-center">
				<RouterProvider router={router} />
			</Stack>
		</Container>
	);
}

export default App;
