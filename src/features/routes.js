import galleryRoutes from "./gallery/routes/index";
import loginRoutes from "./login/routes/index";

export default [galleryRoutes, loginRoutes];
