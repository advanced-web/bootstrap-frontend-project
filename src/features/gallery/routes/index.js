import GalleryFeature from "../index";

export default {
	path: "/",
	element: <GalleryFeature />,
};
