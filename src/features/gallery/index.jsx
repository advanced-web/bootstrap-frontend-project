import { useEffect, useState } from "react";
import { Button, Card, Container, Spinner, Stack } from "react-bootstrap";
import { useNavigate } from "react-router-dom";
import ImgFlipService from "../../services/ImgFlipService";
import GalleryList from "./components/GalleryList/index.";

function GalleryFeature() {
	const [memeList, setMemeList] = useState([]);
	const [isLoading, setIsLoading] = useState(false);
	const navigate = useNavigate();

	const loadImage = async () => {
		try {
			setIsLoading(true);
			const res = await ImgFlipService.getMemeListAsync();
			setIsLoading(false);

			if (res.success) {
				setMemeList(res.data.memes);
				return;
			}

			alert("Something went wrong!");
		} catch (error) {
			console.log(error);
			alert("Something went wrong!");
		}
	};

	useEffect(() => {
		loadImage();
	}, []);

	return (
		<Container fluid className="gallery-container py-4">
			<Card className="mb-4">
				<Card.Body>
					<Stack direction="horizontal" gap={3} className="justify-content-center">
						<h2 className="mb-0">Memes Gallery</h2>

						<Button variant="primary" onClick={loadImage}>
							Reload
						</Button>

						<Button variant="primary" onClick={() => navigate("/login")}>
							Login
						</Button>
					</Stack>
				</Card.Body>
			</Card>

			{isLoading ? (
				<Card>
					<Card.Body>
						<Stack direction="vertical" className="align-items-center">
							<Spinner animation="border" variant="primary" className="mb-3" />
							<p className="text-primary fw-bold fs-6">Fetching data, please wait for a moment...</p>
						</Stack>
					</Card.Body>
				</Card>
			) : (
				<GalleryList imageList={memeList} />
			)}
		</Container>
	);
}

export default GalleryFeature;
