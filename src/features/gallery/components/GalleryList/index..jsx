import Masonry, { ResponsiveMasonry } from "react-responsive-masonry";
import GalleryItem from "../GalleryItem";

function GalleryList(props) {
	const { imageList } = props;

	return (
		<ResponsiveMasonry columnsCountBreakPoints={{ 350: 1, 576: 2, 768: 3, 992: 3, 1200: 4, 1400: 6 }}>
			<Masonry gutter="1.5rem">
				{imageList.map((img) => {
					return <GalleryItem key={img.id} title={img.name} imageUrl={img.url} />;
				})}
			</Masonry>
		</ResponsiveMasonry>
	);
}

export default GalleryList;
