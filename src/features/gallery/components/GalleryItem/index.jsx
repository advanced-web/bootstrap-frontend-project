import { Card } from "react-bootstrap";
import "./styles.css";

function GalleryItem(props) {
	const { imageUrl, title } = props;

	return (
		<Card className="gallery-item">
			<Card.Img src={imageUrl} className="gallery-item-image" loading="lazy" title={title} />

			{/* <div className="gallery-item-overlay p-3">
				<h5 className="gallery-item-title">{title}</h5>
			</div> */}
		</Card>
	);
}

export default GalleryItem;
