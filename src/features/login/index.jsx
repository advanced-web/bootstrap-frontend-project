import { Controller, useForm } from "react-hook-form";
import { Button, Card, Container, Form, Stack } from "react-bootstrap";
import { zodResolver } from "@hookform/resolvers/zod";
import { z } from "zod";
import { useNavigate } from "react-router-dom";

const schema = z.object({
	email: z.string().email(),
	password: z.string().trim().min(8, { message: "Password must be at least 8 characters." }),
});

export default function Login() {
	const {
		control,
		handleSubmit,
		reset,
		formState: { errors },
	} = useForm({
		resolver: zodResolver(schema),
	});
	const navigate = useNavigate();

	const handleLogin = (formValues) => {
		reset();
		alert(`Your information:\nEmail: ${formValues.email}\nPassword: ${formValues.password}`);
	};

	return (
		<Container fluid className="vh-100 d-flex py-4 justify-content-center">
			<Stack direction="horizontal">
				<Card>
					<Card.Body>
						<Stack className="m-4">
							<h2>LOGIN</h2>

							<Form onSubmit={handleSubmit(handleLogin)}>
								<Form.Group className="mb-3">
									<Form.Label>Email address</Form.Label>
									<Controller
										control={control}
										name="email"
										defaultValue=""
										render={({ field: { ref, value, onChange } }) => (
											<Form.Control
												ref={ref}
												value={value}
												onChange={onChange}
												placeholder="Enter email"
												autoFocus
											/>
										)}
									/>

									{errors.email ? (
										<Form.Text className="text-danger">{errors.email.message}</Form.Text>
									) : null}
								</Form.Group>

								<Form.Group className="mb-3">
									<Form.Label>Password</Form.Label>
									<Controller
										control={control}
										name="password"
										defaultValue=""
										render={({ field: { ref, value, onChange } }) => (
											<Form.Control
												ref={ref}
												value={value}
												onChange={onChange}
												type="password"
												placeholder="Enter password"
											/>
										)}
									/>

									{errors.password ? (
										<Form.Text className="text-danger">{errors.password.message}</Form.Text>
									) : null}
								</Form.Group>

								<div className="d-grid gap-3">
									<Button variant="primary" type="submit">
										LOGIN
									</Button>

									<Button variant="link" onClick={() => navigate("/")}>
										GO BACK
									</Button>
								</div>
							</Form>
						</Stack>
					</Card.Body>
				</Card>
			</Stack>
		</Container>
	);
}
