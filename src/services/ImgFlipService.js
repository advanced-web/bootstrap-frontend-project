import HttpService from "./HttpService";

export default class ImgFlipService {
	static async getMemeListAsync() {
		return HttpService.get("/get_memes", {
			baseURL: process.env.REACT_APP_IMGFLIP_API,
		});
	}
}
