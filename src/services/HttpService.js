import axios from "axios";

export const axiosInstance = axios.create({
	baseURL: process.env.REACT_APP_BASE_API_URL,
	headers: {
		"Content-Type": "application/json",
	},
});

// Add a request interceptor
axiosInstance.interceptors.request.use(
	function (config) {
		// Do something before request is sent
		return config;
	},
	function (error) {
		// Do something with request error
		return Promise.reject(error);
	},
);

axiosInstance.interceptors.response.use(
	function (response) {
		return response;
	},
	function (error) {
		return Promise.reject(error);
	},
);

export default class HttpService {
	static async get(path, extraConfig) {
		return this.handleAPIResponse(await axiosInstance.get(path, extraConfig));
	}

	static async post(path, payload, extraConfig) {
		return this.handleAPIResponse(await axiosInstance.post(path, payload, extraConfig));
	}

	static async delete(path, extraConfig) {
		return this.handleAPIResponse(await axiosInstance.delete(path, extraConfig));
	}

	static async put(path, payload, extraConfig) {
		return this.handleAPIResponse(await axiosInstance.put(path, payload, extraConfig));
	}

	static async patch(path, payload, extraConfig) {
		return this.handleAPIResponse(await axiosInstance.patch(path, payload, extraConfig));
	}

	static handleAPIResponse(response) {
		return response.data;
	}
}
